# 2021 年认证杯数学中国数学建模网络挑战赛 B 题

## 介绍
包含了2021 年“认证杯”数学中国数学建模网络挑战赛 B 题（第一阶段）的原始数据，代码以及附件，同时附带文件的说明文档。

解题文档见 CSDN 博客：[https://blog.csdn.net/weixin_42141390/article/details/116722963](https://blog.csdn.net/weixin_42141390/article/details/116722963)


## 代码文件说明
代码文件的运行顺序和说明如下：
1. data_preprocessing.py：数据预处理，包括标准化、T检验、分布检验和一些画图和统计描述代码。
2. machine_learning.py：数据预处理，用机器学习方法填充 B-V
3. deep_learning.py：数据预处理，深度学习方法填充 B-V，效果太差，可以不运行。
4. finding_bi_star.py：聚类方法选出毕星团，包括聚类参数筛选等。
5. solve_q2.py：找出毕宿星流
6. describe_with_chart.py：画出位置、速度分布图。


## 其他
一些代码问题和原理问题，可以到博客评论区留言，请不要私信，一般不回。