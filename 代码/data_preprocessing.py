# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import pickle
from scipy.stats import ttest_ind
from scipy.stats import kruskal
from sklearn.preprocessing import StandardScaler

def describe_data(data_for_clu, data_condition, columns):
    '''
    描述星体数据
    '''
    print('原始数据的统计描述： ', data_for_clu.describe())
    print('Plx 落在[20，22]的星体数据的统计描述： ', data_condition.describe())

    
    # 进行 T 检验
    for col in columns:
        # 遍历每一列
        x1 = data_condition[col]
        x2 = data_for_clu[col]
        t, p_value = ttest_ind(x1, x2)
        if p_value >= 0.05:
            print(f'对于 {col} 列，原假设不能被拒绝，所以大致认为： + \
                    全部数据和[20,22]数据的{col}列没有显著差异')
        else:
            print(f'对于 {col} 列，原假设被拒绝，所以有95%的把握认为：+ \
            全部数据和[20,22]数据的{col}列有显著差异')
    
    # 进行 Kruskal-Wallis H 分布检验
    for col in columns:
        # 遍历每一列
        x1 = data_condition[col]
        x2 = data_for_clu[col]
        t, p_value = kruskal(x1, x2)
        if p_value >= 0.05:
            print(f'对于 {col} 列，原假设不能被拒绝，所以大致认为： + \
                    全部数据和[20,22]数据的{col}列分布相同')
        else:
            print(f'对于 {col} 列，原假设被拒绝，所以有95%的把握认为： + \
                    全部数据和[20,22]数据的{col}列分布相同')
                    
    # 数据量
    print('数据量： ', data_for_clu.shape[0], '行')
    
    # 输出缺失数据
    nan_count = dict()
    for col in columns:
        nan_count[col] = df[col].isna().sum()
    print(r'每一列的缺失数据： ', nan_count)

if __name__ == '__main__':
    path = r'../附件/星表数据.txt'
    # 读取 txt 文件
    df = pd.read_csv(path, delimiter=' +')
    # 删除 星体编号一行
    data_for_clu = df.drop('HIP', axis=1)
    # 落在 [20，22] 的数据
    data_condition = data_for_clu.loc[(20<=data_for_clu['Plx']) & (data_for_clu['Plx']<=22)]
    # 描述数据
    columns = data_for_clu.columns
    describe_data(data_for_clu, data_condition, columns)
    
    # 数据标准化
    scaler = StandardScaler()
    data_after = scaler.fit_transform(data_for_clu)
    data_after = pd.DataFrame(data=data_after, columns=columns)
    
    # 保存数据
    file_path = r'../附件/intermedia_data.pkl'
    scaler_model = r'../附件/scaler_model.pkl'
    pickle.dump(data_after, open(file_path, 'wb'))
    pickle.dump(scaler, open(scaler_model, 'wb'))

                    
    
